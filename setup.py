#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''XCGF''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1489189554)),
  description='''Xyne's common generic functions, for internal use.''',
  author='''Xyne''',
  author_email='''ac xunilhcra enyx, backwards''',
  url='''http://xyne.archlinux.ca/projects/python3-xcgf''',
  py_modules=['''XCGF'''],
)
